# el-baraton

> Frontend Developer Challenge

Ejemplo en línea [El baratón](http://el-baraton.developers.ninja/)

## Build Setup


## Todas las dependencias están incluidas en el package.json, sólo se deben ejecutar los siguientes comandos para ejecutar la aplicacion
``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
