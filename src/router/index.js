import Vue from 'vue'
import Router from 'vue-router'
import Shop from '@/components/Shop'
import NotFoundComponent from '@/components/NotFoundComponent'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
	{
	 	path: '/',
	 	name: 'Shop',
	 	component: Shop
	},
  	{	
  		path: '*',
  		component: NotFoundComponent
  	}
  ]
})
