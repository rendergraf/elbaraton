import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex);
import getJsonProducts from '../../assets/data/products.json'
const products = getJsonProducts.products

import getJson from '../../assets/data/categories.json'
const jsonList = ('json',getJson)
const categories = jsonList.categories

export const store = new Vuex.Store({
	state: {
		productsList: products,
		listCategory: categories,
		compra: []
	}
})