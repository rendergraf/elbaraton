// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import Resource from 'vue-resource';
import VueLocalStorage from 'vue-localstorage'
import {VueFeatherIconsSsr as icon} from 'vue-feather-icons-ssr'
import App from './App'
import VueLazyload from 'vue-lazyload'
import router from './router'
import { store } from './store/store'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue);
Vue.use(Resource);
Vue.use(VueLocalStorage);
Vue.component('icon', icon);
Vue.use(VueLazyload);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
	el: '#app',
	store,
	router,
	template: '<App/>',
	components: { App }
})
